AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

local cv_redCarpetAdminOnly = CreateConVar("sv_carpets_red_admin_only", 0, FCVAR_NONE, "Should be red carpet avaible only for admins? Default is 0.")
local cv_redCarpetIgnitionTime = CreateConVar("sv_carpets_red_ignition_time", 10, FCVAR_NEVER_AS_STRING, "Amount of time that red carpet ignites things. Default is 10.")
local cv_redCarpetIgnitionRadius = CreateConVar("sv_carpets_red_ignition_radius", 0, FCVAR_NEVER_AS_STRING, "Radius that makes ignited object ignite others around. 0 to not ignite anything around. Default is 0.")

ENT.Spawnable		            			= true
ENT.AdminOnly		           		    	= false 
ENT.PrintName		                		= "Red Carpet (Ignition)"
ENT.Author			                	 	= "eLeCtrOssSnake"
ENT.Contact			                	 	= "electrosssnake@mail.ru"
ENT.Category                         		= "ES Magic Carpets"
ENT.IgnitionTime					 		= cv_redCarpetIgnitionTime:GetInt()
ENT.IgnitionRadius							= cv_redCarpetIgnitionRadius:GetInt()

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(143, 30, 30))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end

function ENT:StartTouch(entity)
	if(entity:IsValid()) then
		local shouldIgnite = true
		if(entity.unFreezeTimeC != nil) then
			if(entity.unFreezeTimeC > CurTime()) then
				shouldIgnite = false
		    end
		end
		if(shouldIgnite) then
			entity:Ignite(self.IgnitionTime, self.IgnitionRadius)
	 	end
	end
end
