--[[
AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

ENT.Spawnable		            	 =  true
ENT.AdminOnly		           		 = false 
ENT.PrintName		                 =  "Brown Carpet (Digging)"
ENT.Author			                 =  "eLeCtrOssSnake"
ENT.Contact			                 =  "electrosssnake@mail.ru"
ENT.Category                         =  "ES Magic Carpets"

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(222,184,135))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end
--]]