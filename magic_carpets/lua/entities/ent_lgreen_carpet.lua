AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

local cv_lgreenCarpetAdminOnly = CreateConVar("sv_carpets_lgreen_admin_only", 0, FCVAR_NONE, "Should be lgreen carpet avaible only for admins? Default is 0.")
local cv_lgreenCarpetPreset = CreateConVar("sv_carpets_lgreen_preset", 1, FCVAR_NEVER_AS_STRING, "Should lgreen carpet use preset values on spawn else it will use random. Default is 1.")
local cv_lgreenCarpetHPerHeal = CreateConVar("sv_carpets_lgreen_hp_per_heal", 1, FCVAR_NEVER_AS_STRING, "Amount of health that lgreen carpet heals in one delay. Default is 1.")
local cv_lgreenCarpetHPAmount = CreateConVar("sv_carpets_lgreen_hp_amount", 150, FCVAR_NEVER_AS_STRING, "Amount of health that lgreen carpet give to things. Default is 150.")
local cv_lgreenCarpetHealDelay = CreateConVar("sv_carpets_lgreen_heal_delay", 0.25, FCVAR_NEVER_AS_STRING, "Amount of time that lgreen carpet heal again. Default is 0.25.")
local cv_lgreenCarpetShowHPAmount = CreateConVar("sv_carpets_lgreen_show_hp_amount", 1, FCVAR_NONE, "Should lgreen carpet show his hp ammount left. Default is 1.")

ENT.Spawnable		            	= true
ENT.AdminOnly		           		= cv_lgreenCarpetAdminOnly:GetBool() 
ENT.PrintName		               	= "Green Carpet (LHealing)"
ENT.Author			               	= "eLeCtrOssSnake"
ENT.Contact			               	= "electrosssnake@mail.ru"
ENT.Category                       	= "ES Magic Carpets"
ENT.Editable 						= true
ENT.NextHeal						= CurTime()
ENT.TextColor 						= Color(33, 33, 33)

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(0, 255, 0))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject() // physics init
	if (IsValid(phys)) then phys:Wake() end
	if(!cv_lgreenCarpetPreset:GetBool()) then
		self:SetHP_PerHeal(math.random(1, 12))
		self:SetHP_Amount(math.random(50, 5000))
		self:SetHeal_Delay(math.random(0.1, 3))
	else
		self:SetHP_PerHeal(cv_lgreenCarpetHPerHeal:GetInt())
		self:SetHP_Amount(cv_lgreenCarpetHPAmount:GetInt())
		self:SetHeal_Delay(cv_lgreenCarpetHealDelay:GetFloat())
	end
	self.TextColor = Color(math.random(0, 255), math.random(0, 255), math.random(0, 255))
	self:SetShow_HP_Amount(cv_lgreenCarpetShowHPAmount:GetBool())
end

function ENT:SetupDataTables()
	self:NetworkVar( "Int", 	3, "HP_PerHeal", 	{ KeyName = "kHPPerHeal", 	Edit = { type = "Int", 		order = 3, min = 1, max = 1000 } } )
	self:NetworkVar( "Float", 	4, "Heal_Delay", 	{ KeyName = "KHealDelay", 	Edit = { type = "Float", 	order = 4, min = 0.01, max = 5 } } )
	self:NetworkVar( "Int", 	2, "HP_Amount", 	{ KeyName = "kHPAmount", 	Edit = { type = "Int", 		order = 2, min = 1, max = 10000 } } )
	self:NetworkVar( "Bool", 	1, "Show_HP_Amount", 	{ KeyName = "kShowHPAmount", 	Edit = { type = "Bool", 		order = 1} } )
end

function ENT:Touch(entity)
	if((entity:IsValid() and (entity:IsPlayer() or entity:IsNPC())) and self.NextHeal <= CurTime()) then
		if(entity:Health() == entity:GetMaxHealth()) then return end
		if(!(self:GetHP_Amount() <= 0) and entity:IsValid() and (entity:IsPlayer() or entity:IsNPC()) and (entity:GetMaxHealth() >= (entity:Health() - 1))) then
			if((entity:GetMaxHealth() - (entity:Health() + self:GetHP_PerHeal())) < self:GetHP_PerHeal()) then
				self:SetHP_Amount(self:GetHP_Amount() - (entity:GetMaxHealth() - entity:Health()))
				entity:SetHealth(entity:GetMaxHealth())
			else
				entity:SetHealth(entity:Health() + self:GetHP_PerHeal())
				self:SetHP_Amount(self:GetHP_Amount() - self:GetHP_PerHeal())
			end
			self.NextHeal = CurTime() + self:GetHeal_Delay()
		end
	end
end

function ENT:Draw()
	self:DrawModel()
	--calcAngle.pitch = 90
	if(self:GetShow_HP_Amount() and self.MatTime != 0) then
		cam.Start3D2D(self:GetPos() + Vector(0, 0, 1), self:GetAngles(), 1)
			local value = nil
			if(self:GetHP_Amount() <= 0) then
				value = "out"
			else
				value = "" .. self:GetHP_Amount()
			end
			draw.SimpleText(value, "DermaLarge", 0, 0, self.TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		cam.End3D2D()
	end
end