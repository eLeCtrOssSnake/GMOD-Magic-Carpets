AddCSLuaFile()
if( SERVER ) then -- It should not have been shared
    include("es/carpets/main.lua") -- So I don't need to create autorun
end

DEFINE_BASECLASS( "base_anim" )

ENT.Spawnable		            			  = true
ENT.AdminOnly		           		    	= false 
ENT.PrintName		                		= "LBlue Carpet (Freeze)"
ENT.Author			                	 	= "eLeCtrOssSnake"
ENT.Contact			                	 	= "electrosssnake@mail.ru"
ENT.Category                        = "ES Magic Carpets"

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(0, 199, 255))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end

function ENT:Touch(entity)
    if CLIENT then return end
	if(entity:IsValid() and entity:IsPlayer()) then
        if(entity:IsFrozenByCarpet() == true) then
        	--print("DBG_ALREADY_FROZEN")
            entity:Extinguish()
       	else
       		local inTable = false
       		--print("DBG_FROZE")
        	entity:FreezeWithCarpet()
        	for k, v in pairs(es.carpets.frozen) do
        		if(v:Nick() == entity:Nick()) then
        			inTable = true
        		end
        	end
        	if(!inTable) then
       			table.insert(es.carpets.frozen, entity)
       	    end
       		entity.unFreezeTimeC = CurTime() + 10
        end
	end
end
