AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

local cv_blackCarpetAdminOnly = CreateConVar("sv_carpets_black_admin_only", 0, FCVAR_NONE, "Should be black carpet avaible only for admins? Default is 0.")

ENT.Spawnable		            			= true
ENT.AdminOnly		           		    	= cv_blackCarpetAdminOnly:GetBool() 
ENT.PrintName		                		= "Black Carpet (Disintigration)"
ENT.Author			                	 	= "eLeCtrOssSnake"
ENT.Contact			                	 	= "electrosssnake@mail.ru"
ENT.Category                         		= "ES Magic Carpets"

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(24, 22, 22))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end

function ENT:StartTouch(entity)
	if(entity:IsValid()) then
		local damageInfo = DamageInfo()
        damageInfo:SetDamage(math.huge)
        damageInfo:SetDamageType(DMG_DISSOLVE)
        damageInfo:SetAttacker(self)
        damageInfo:SetInflictor(self)
        damageInfo:SetDamageForce(Vector(0, 0, math.random(10,50)))
        damageInfo:SetDamagePosition(entity:GetPos())
        entity:TakeDamageInfo(damageInfo)
	end
end
