AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

local cv_greyCarpetAdminOnly = CreateConVar("sv_carpets_grey_admin_only", 0, FCVAR_NONE, "Should be grey carpet avaible only for admins? Default is 0.")
local cv_greyCarpet_ammoPerSupply = CreateConVar("sv_carpets_grey_ammo_per_supply", 10, FCVAR_NEVER_AS_STRING, "How much ammo per supply carpet should give, set to 0 to randomise. Default is 10.")
local cv_greyCarpet_supplyDelay = CreateConVar("sv_carpets_grey_supply_delay", 1, FCVAR_NEVER_AS_STRING, "Delay between supplies, set to 0 to randomise. Default is 1.")

ENT.Spawnable		            			= true
ENT.AdminOnly		           		    	= cv_greyCarpetAdminOnly:GetBool()
ENT.Editable 								= true
ENT.PrintName		                		= "Grey Carpet (Ammo supply)"
ENT.Author			                	 	= "eLeCtrOssSnake"
ENT.Contact			                	 	= "electrosssnake@mail.ru"
ENT.Category                         		= "ES Magic Carpets"
ENT.NextSupply								= CurTime()

function ENT:SetupDataTables()
	self:NetworkVar( "Int", 	3, "AmmoPerSupply", { KeyName = "kAmmoPerSupply", 	Edit = { type = "Int", 	 order = 1, min = 1, 	max = 500 } } )
	self:NetworkVar( "Float", 	2, "SupplyDelay", 	{ KeyName = "kSupplyDelay", 	Edit = { type = "Float", order = 2, min = 0.1,  max = 120 } } )
	self:NetworkVar( "Bool", 	1, "HidePopup", 	{ KeyName = "kHidePopup", 		Edit = { type = "Bool",  order = 3} } )
end

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(121, 118, 116))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
	if( cv_greyCarpet_ammoPerSupply:GetInt() == 0 ) then
		self:SetAmmoPerSupply( math.random( 1, 500 ) )
	else
		self:SetAmmoPerSupply( cv_greyCarpet_ammoPerSupply:GetInt() )
	end

	if( cv_greyCarpet_supplyDelay:GetFloat() == 0 ) then
		self:SetSupplyDelay( math.Rand( 0.1, 120 ) )
	else
		self:SetSupplyDelay( cv_greyCarpet_supplyDelay:GetFloat() )
	end
end

function ENT:Touch(entity)
	if( self.NextSupply < CurTime() and IsValid( entity ) and entity:IsPlayer() ) then
		local wep = entity:GetActiveWeapon()

		if( IsValid( wep ) and wep:GetMaxClip1() != -1 and wep:GetMaxClip1() != 0 ) then
			entity:GiveAmmo( self:GetAmmoPerSupply(), wep:GetPrimaryAmmoType(), self:GetHidePopup() )
			self.NextSupply = CurTime() + self:GetSupplyDelay()
		end
	end
end