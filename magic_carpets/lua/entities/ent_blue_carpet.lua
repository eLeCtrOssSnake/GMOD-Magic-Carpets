AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

ENT.Spawnable		            	    = true
ENT.AdminOnly		           		    = false 
ENT.PrintName		                    = "Blue Carpet (Electricity)"
ENT.Author			                	= "eLeCtrOssSnake"
ENT.Contact			                	= "electrosssnake@mail.ru"
ENT.Category                         	= "ES Magic Carpets"
ENT.Editable                            = true
ENT.NextDamage                          = CurTime()
ENT.Sounds                              = {"ambient/energy/spark", "ambient/energy/zap"}

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(46, 39, 255))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
    if (IsValid(phys)) then phys:Wake() end
	
    self:SetDamage( 4 )
    self:SetDamageDelay( 0.13 )
end

function ENT:SetupDataTables()
    self:NetworkVar( "Int",     1, "Damage",        { KeyName = "kDamage",      Edit = { type = "Int",   order = 1, min = 1,    max = 500 } } )
    self:NetworkVar( "Float",   2, "DamageDelay",   { KeyName = "kDamageDelay", Edit = { type = "Float", order = 2, min = 0.1,  max = 120 } } )
end

function ENT:Touch(entity)
    if(entity:IsValid()) then
        if(self.NextDamage <= CurTime()) then
            local damageInfo = DamageInfo()
            damageInfo:SetDamage( self:GetDamage() )
            damageInfo:SetDamageType(DMG_SHOCK)
            damageInfo:SetAttacker(self)
            damageInfo:SetInflictor(self)
            damageInfo:SetDamageForce(Vector(0, 0, math.random(10,50)))
            damageInfo:SetDamagePosition(entity:GetPos() + Vector(math.random(-16, 16), math.random(-16, 16), math.random(0, 64)))
            entity:TakeDamageInfo(damageInfo)

            if(entity:IsPlayer()) then
                entity:ViewPunch(Angle(math.random(-16, 20), math.random(-25, 30), math.random(-7, 16)))
            end
            self.NextDamage = CurTime() + self:GetDamageDelay()
            local randomSound = math.random(1, 9)
            if(randomSound <= 6) then -- spark has only 6 variants
                self:EmitSound( self.Sounds[math.random(1, 2)] .. randomSound .. ".wav", 75, 100, 1, CHAN_AUTO)
            else -- if sound variant bigger than 6 we want play only zap
                self:EmitSound( self.Sounds[2] .. randomSound .. ".wav", 75, 100, 1, CHAN_AUTO)
            end
            local effectdata = EffectData()
            effectdata:SetOrigin(self:GetPos() + Vector(math.random(0, 20), math.random(0, 20), 0))
            effectdata:SetAngles(self:GetAngles())
            util.Effect("ManhackSparks", effectdata)
        end
    end
end