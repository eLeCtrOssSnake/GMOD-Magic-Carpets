AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

local cv_lorangeCarpetAdminOnly = CreateConVar("sv_carpets_lgreen_admin_only", 0, FCVAR_NONE, 				"Should be lorange carpet available only for admins? Default is 0.")
local cv_lorangeCarpetPreset = CreateConVar("sv_carpets_lgreen_preset", 1, FCVAR_NEVER_AS_STRING, 			"Should lorange carpet use preset values on spawn else it will use random.")
local cv_lorangeCarpetARPerGive = CreateConVar("sv_carpets_lgreen_hp_per_heal", 1, FCVAR_NEVER_AS_STRING, 	"Amount of armor that lorange carpet gives in one delay. Default is 1.")
local cv_lorangeCarpetARAmount = CreateConVar("sv_carpets_lgreen_hp_amount", 150, FCVAR_NEVER_AS_STRING, 	"Amount of armor that lorange carpet give to things. Default is 150.")
local cv_lorangeCarpetGiveDelay = CreateConVar("sv_carpets_lgreen_heal_delay", 0.25, FCVAR_NEVER_AS_STRING, "Amount of time that lorange carpet give again. Default is 0.25.")
local cv_lorangeCarpetShowARAmount = CreateConVar("sv_carpets_lgreen_show_hp_amount", 1, FCVAR_NONE, 		"Should lorange carpet show his ar amount left. Default is 1.")

ENT.Spawnable		            	= true
ENT.AdminOnly		           		= cv_lorangeCarpetAdminOnly:GetBool() 
ENT.PrintName		               	= "Orange Carpet (LArmoring)"
ENT.Author			               	= "eLeCtrOssSnake"
ENT.Contact			               	= "electrosssnake@mail.ru"
ENT.Category                       	= "ES Magic Carpets"
ENT.Editable 						= true
ENT.NextGive						= CurTime()
ENT.TextColor 						= Color(33, 33, 33)

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(255, 163, 60)) // set orange color

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject() // physics init
	if (IsValid(phys)) then phys:Wake() end
	if(!cv_lorangeCarpetPreset:GetBool()) then
		self:SetAR_PerGive(math.random(1, 12))
		self:SetAR_Amount(math.random(50, 5000))
		self:SetGive_Delay(math.random(0.1, 3))
	else
		self:SetAR_PerGive(cv_lorangeCarpetARPerGive:GetInt())
		self:SetAR_Amount(cv_lorangeCarpetARAmount:GetInt())
		self:SetGive_Delay(cv_lorangeCarpetGiveDelay:GetFloat())
	end
	self.TextColor = Color(math.random(0, 255), math.random(0, 255), math.random(0, 255))
	self:SetShow_AR_Amount(cv_lorangeCarpetShowARAmount:GetBool())
end

function ENT:SetupDataTables()
	self:NetworkVar( "Int", 	3, "AR_PerGive", 	{ KeyName = "kARPerGive", 	Edit = { type = "Int", 		order = 3, min = 1, max = 1000 } } )
	self:NetworkVar( "Float", 	4, "Give_Delay", 	{ KeyName = "KGiveDelay", 	Edit = { type = "Float", 	order = 4, min = 0.01, max = 5 } } )
	self:NetworkVar( "Int", 	2, "AR_Amount", 	{ KeyName = "kARAmount", 	Edit = { type = "Int", 		order = 2, min = 1, max = 10000 } } )
	self:NetworkVar( "Bool", 	1, "Show_AR_Amount", 	{ KeyName = "kShowARAmount", 	Edit = { type = "Bool", 		order = 1} } )
end

function ENT:Touch(entity)
	if((entity:IsValid() and (entity:IsPlayer() or entity:IsNPC())) and self.NextGive <= CurTime()) then
		if(entity:Armor() == 100) then return end
		if(!(self:GetAR_Amount() <= 0) and entity:IsValid() and (entity:IsPlayer() or entity:IsNPC()) and (100 >= (entity:Armor() - 1))) then
			if((100 - (entity:Armor() + self:GetAR_PerGive())) < self:GetAR_PerGive()) then
				self:SetAR_Amount(self:GetAR_Amount() - (100 - entity:Armor()))
				entity:SetArmor(100)
			else
				entity:SetArmor(entity:Armor() + self:GetAR_PerGive())
				self:SetAR_Amount(self:GetAR_Amount() - self:GetAR_PerGive())
			end
			self.NextGive = CurTime() + self:GetGive_Delay()
		end
	end
end

function ENT:Draw()
	self:DrawModel()
	--calcAngle.pitch = 90
	if(self:GetShow_AR_Amount() and self.MatTime != 0) then
		cam.Start3D2D(self:GetPos() + Vector(0, 0, 1), self:GetAngles(), 1)
			local value = nil
			if(self:GetAR_Amount() <= 0) then
				value = "out"
			else
				value = "" .. self:GetAR_Amount()
			end
			draw.SimpleText(value, "DermaLarge", 0, 0, self.TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		cam.End3D2D()
	end
end