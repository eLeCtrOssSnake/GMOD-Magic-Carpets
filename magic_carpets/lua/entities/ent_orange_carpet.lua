AddCSLuaFile()

DEFINE_BASECLASS("base_anim")

local cv_orangeCarpetAdminOnly = CreateConVar("sv_carpets_orange_admin_only", 1, FCVAR_NONE, "Should be orange carpet avaible only for admins? Default is 1.")
local cv_orangeCarpetOrangeMaxArmor = CreateConVar("sv_carpets_orange_max_armor", 100, FCVAR_NEVER_AS_STRING, "Max armor to restore by orange carpet. Default is 100.")

ENT.Spawnable		            	 = true
ENT.AdminOnly		           		 = cv_orangeCarpetAdminOnly:GetBool()
ENT.PrintName		                 = "Orange Carpet (Armor)"
ENT.Author			                 = "eLeCtrOssSnake"
ENT.Contact			                 = "electrosssnake@mail.ru"
ENT.Category                         = "ES Magic Carpets"

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(255, 163, 60)) // set orange color

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject() // physics init
	if (IsValid(phys)) then phys:Wake() end
	
end

function ENT:Touch(entity)
	if(entity:IsValid() and (entity:IsPlayer() or entity:IsNPC())) then
		if(entity:Armor() < cv_orangeCarpetOrangeMaxArmor:GetInt()) then
			entity:SetArmor(entity:Armor() + 1)
		end
	end
end