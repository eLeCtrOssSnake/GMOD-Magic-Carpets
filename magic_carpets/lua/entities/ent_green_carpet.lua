AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

local cv_greenCarpetAdminOnly = CreateConVar("sv_carpets_green_admin_only", 1, FCVAR_NONE, "Should be green carpet avaible only for admins? Default is 1.")

ENT.Spawnable		            	 = true
ENT.AdminOnly		           		 = cv_greenCarpetAdminOnly:GetBool()
ENT.PrintName		                 = "Green Carpet (Healing)"
ENT.Author			                 = "eLeCtrOssSnake"
ENT.Contact			                 = "electrosssnake@mail.ru"
ENT.Category                         = "ES Magic Carpets"

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(0, 255, 0))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end

function ENT:Touch(entity)
	if(entity:IsValid() and (entity:IsPlayer() or entity:IsNPC())) then
		if(entity:Health() < entity:GetMaxHealth()) then
			entity:SetHealth(entity:Health() + 1)
		end
	end
end