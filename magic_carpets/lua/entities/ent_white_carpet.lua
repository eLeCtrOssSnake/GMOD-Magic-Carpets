AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

local cv_whiteCarpetAdminOnly = CreateConVar("sv_carpets_black_admin_only", 0, FCVAR_NONE, "Should be white carpet avaiable only for admins? Default is 0.")

ENT.Spawnable		            			= true
ENT.AdminOnly		           		    	= cv_whiteCarpetAdminOnly:GetBool() 
ENT.PrintName		                		= "White Carpet (Blinds)"
ENT.Author			                	 	= "eLeCtrOssSnake"
ENT.Contact			                	 	= "electrosssnake@mail.ru"
ENT.Category                         		= "ES Magic Carpets"
ENT.Editable 								= true

function ENT:SetupDataTables()
	self:NetworkVar( "Float", 	1, "BlindTime", 	{ KeyName = "kBlindTime", 	Edit = { type = "Float",  order = 1, min = 1, max = 100 } } )
	self:NetworkVar( "Int", 	2, "FadeSpeed", 	{ KeyName = "kFadeSpeed", 	Edit = { type = "Int",    order = 2, min = 1, max = 255 } } )
	self:NetworkVar( "Bool", 	3, "PlaySound", 	{ KeyName = "kPlaySound", 	Edit = { type = "Bool",   order = 3 } } )
	--self:NetworkVar( "String", 	4, "SoundPath", 	{ KeyName = "kSoundPath", 	Edit = { type = "String", order = 4 } } )

	if SERVER then
		self:SetBlindTime( 4 )
		self:SetFadeSpeed( 5 )
		self:SetPlaySound( true )
		--self:SetSoundPath( "audio or something" )
	end
end

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end

function ENT:StartTouch(entity)
	if( entity:IsPlayer() ) then
		umsg.Start("Carpets.BlindPlayer", entity)
			umsg.Short( self:GetBlindTime() )
			umsg.Short( self:GetFadeSpeed() )
		umsg.End()
	end
end

if CLIENT then

	local function BlindPlayer( msgArgs )
		local blindTime = msgArgs:ReadShort()
		local fadeSpeed = msgArgs:ReadShort()
		local ply = LocalPlayer()

		ply.carpetBlinded = true
		ply.carpetRemoveBlindTime = CurTime() + blindTime
		ply.carpetFadeSpeed = fadeSpeed
		ply.carpetBlindAlpha = 255
	end
	usermessage.Hook( "Carpets.BlindPlayer", BlindPlayer )

	local function DrawOverlay()
		local ply = LocalPlayer()

		if ply.carpetBlinded == true then
			surface.SetDrawColor( 255, 255, 255, ply.carpetBlindAlpha )
			surface.DrawRect( 0, 0, ScrW(), ScrH() )
		end

		if ply.carpetBlinded == true and ply.carpetRemoveBlindTime < CurTime() then
			ply.carpetBlindAlpha = ply.carpetBlindAlpha - ply.carpetFadeSpeed

			if( ply.carpetBlindAlpha <= 0 ) then
				ply.carpetBlinded = nil
				ply.carpetRemoveBlindTime = nil
				ply.carpetFadeSpeed = nil
				ply.carpetBlindAlpha = nil
			end
		end
	end
	hook.Add( "DrawOverlay", "DrawBlindness", DrawOverlay)
end