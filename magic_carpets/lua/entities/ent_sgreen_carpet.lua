AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

local cv_sgreenCarpetAdminOnly = CreateConVar("sv_carpets_sgreen_admin_only", 0, FCVAR_NONE, "Should be sgreen carpet avaible only for admins? Default is 0.")
local cv_sgreenCarpetPreset = CreateConVar("sv_carpets_sgreen_preset", 1, FCVAR_NEVER_AS_STRING, "Should sgreen carpet use preset jump boost on spawn else it will use random. Default is 1.")
local cv_sgreenCarpetJumpBoost = CreateConVar("sv_carpets_sgreen_jump_boost", 400, FCVAR_NEVER_AS_STRING, "How much jump boost carpet should give. Default is 400.")
local cv_sgreenCarpetJumpBoostTime = CreateConVar("sv_carpets_sgreen_jump_boost_time", 1, FCVAR_NEVER_AS_STRING, "How much should jump boost last on player. Default is 1.")

ENT.Spawnable		            	= true
ENT.AdminOnly		           		= cv_sgreenCarpetAdminOnly:GetBool() 
ENT.PrintName		               	= "Green Carpet (Slime)"
ENT.Author			               	= "eLeCtrOssSnake"
ENT.Contact			               	= "electrosssnake@mail.ru"
ENT.Category                       	= "ES Magic Carpets"
ENT.Editable 						= true
ENT.PointA 							= Vector( 0, 0, 0 )
ENT.PointB 							= Vector( 0, 0, 0 )

function ENT:Initialize()
	self:SetModel("models/electrosssnake/carpet.mdl")

	self:SetColor(Color(0, 200, 0))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject() // physics init
	if (IsValid(phys)) then phys:Wake() end

	if(!cv_sgreenCarpetPreset:GetBool()) then
		self:SetJumpBoost(math.random(25, 500))
		self:SetJumpBoostTime(math.random(1, 60))
	else
		self:SetJumpBoost(cv_sgreenCarpetJumpBoost:GetInt())
		self:SetJumpBoost(cv_sgreenCarpetJumpBoostTime:GetFloat())
	end
end

function ENT:SetupDataTables()
	self:NetworkVar( "Int", 	2, "JumpBoost", 		{ KeyName = "kJumpBoost", 		Edit = { type = "Int", 	 order = 1, min = 280,  max = 5000 } } )
	self:NetworkVar( "Float", 	1, "JumpBoostTime", 	{ KeyName = "kJumpBoostTime", 	Edit = { type = "Float", order = 2, min = 0.01, max = 3600 } } )
end

function ENT:CalcOffsets() -- Custom
	local pos = self:GetPos()
    local ang = self:GetAngles()
    pos = pos + -ang:Up() * 1.5
    pos = pos + ang:Forward() * 36.15
    pos = pos + ang:Right() * 21
    self.PointA = pos
    pos = self:GetPos()
    pos = pos + ang:Up() * 1.4
    pos = pos + -ang:Forward() * 36.15
    pos = pos + -ang:Right() * 21
    self.PointB = pos
end

function ENT:Think()
	if CLIENT then return end
	self:CalcOffsets()

	local found = ents.FindInBox( self.PointA, self.PointB )
	for k, v in pairs( found ) do
		if( IsValid(v) and v:IsPlayer() ) then
			v:JumpBoost( self:GetJumpBoost(), self:GetJumpBoostTime() )
		end
	end
end

function ENT:Draw()
	self:DrawModel()
end