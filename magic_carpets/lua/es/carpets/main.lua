if(es == nil) then
	es = {}
end
if(es.carpets == nil) then
	es.carpets = {}
end
if(es.carpets.frozen == nil) then
	es.carpets.frozen = {}
end
es.carpets.jumpboosted = es.carpets.jumpboosted or {}

es.carpets.matOverride = "electrosssnake/ice"
local meta = FindMetaTable("Player")

function meta:SetBadStats()
	self:SetWalkSpeed(self:GetWalkSpeed() / 10)
	self:SetRunSpeed(self:GetRunSpeed() / 10)
	self:SetJumpPower(self:GetJumpPower() / 10)
end

function meta:SavePrevStats()
	self.PrevWalkSpd = self:GetWalkSpeed()
	self.PrevRunSpd = self:GetRunSpeed()
	self.PrevJumpPwr = self:GetJumpPower()
end

function meta:SetPrevStats()
	self:SetWalkSpeed(self.PrevWalkSpd)
	self:SetRunSpeed(self.PrevRunSpd)
	self:SetJumpPower(self.PrevJumpPwr)
end

function meta:IsFrozenByCarpet()
	return self.carpetFrozen
end

function meta:FreezeWithCarpet()
	self.carpetFrozen = true
	self:SavePrevStats()
	self:SetBadStats()
	self:SetMaterial(es.carpets.matOverride, false)
end

function meta:UnFreezeWithCarpet()
	self.carpetFrozen = false
	self:SetPrevStats()
	self:SetMaterial("", false)
end

function meta:JumpBoost( newPower, time )
	if( self.PrevJumpPwr != nil and self.PrevJumpPwr == newPower ) then return end
	self.PrevJumpPwr = self:GetJumpPower()
	self:SetJumpPower( newPower )
	self.newJumpPwr = newPower
	self.resetJumpBoostTimeC = CurTime() + time
	table.insert( es.carpets.jumpboosted, self )
end

function meta:ResetJumpBoost()
	if( self.PrevJumpPwr == nil ) then return end
	self:SetJumpPower( self.PrevJumpPwr )
	self.PrevJumpPwr = nil
	self.newJumpPwr = nil
end

function ThinkLoop()
	for k, v in pairs(es.carpets.frozen) do
		if(v.unFreezeTimeC <= CurTime()) then
    		v:UnFreezeWithCarpet()
    		es.carpets.frozen[k] = nil
		end
	end
	for k, v in pairs( es.carpets.jumpboosted ) do
		if( v.resetJumpBoostTimeC <= CurTime() ) then
			v:ResetJumpBoost()
			es.carpets.jumpboosted[k] = nil
		end
	end
end
hook.Add("Think", "carpets.Think", ThinkLoop)