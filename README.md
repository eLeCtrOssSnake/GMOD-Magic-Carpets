[![pipeline status](https://gitlab.com/eLeCtrOssSnake/GMOD-Magic-Carpets/badges/master/pipeline.svg)](https://gitlab.com/eLeCtrOssSnake/GMOD-Magic-Carpets/commits/master)
# [GMOD] Magic Carpets
Addon for gmod that adds ten carpets that got different powers.
Addon also contents a carpet model by me.
If you want download addon it self you need copy magic_carpets folder into your garrysmod/addons folder. Or you can see the sources of models and materials.
Created by eLeCtrOssSnake.
Contact electrosssnake@mail.ru or on steam https://steamcommunity.com/id/eLeCtrOssSnake/
